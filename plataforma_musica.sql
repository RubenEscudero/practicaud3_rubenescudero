CREATE DATABASE plataforma_musica;

USE plataforma_musica;

CREATE TABLE grupo(
id INT AUTO_INCREMENT PRIMARY KEY,
nombre_grupo VARCHAR(50),
fecha_creacion DATE
);

CREATE TABLE cancion(
id INT AUTO_INCREMENT PRIMARY KEY,
nombre_cancion VARCHAR(50),
duracion INT,
id_grupo int REFERENCES grupo (id)
);

CREATE TABLE album(
id INT AUTO_INCREMENT PRIMARY KEY,
nombre_album VARCHAR(50),
fecha_lanzamiento DATE,
id_cancion int references cancion (id)
);

CREATE TABLE artista(
id INT AUTO_INCREMENT PRIMARY KEY,
nombre_artista VARCHAR(50),
fecha_nacimiento DATE,
id_album int REFERENCES album (id)
);

CREATE TABLE instrumento(
id INT AUTO_INCREMENT PRIMARY KEY,
nombre_instrumento VARCHAR(50),
material VARCHAR(50),
id_artista int REFERENCES artista(id)
);

CREATE TABLE ciudad(
id INT AUTO_INCREMENT PRIMARY KEY,
nombre_ciudad VARCHAR(50),
pais VARCHAR(50),
id_grupo int REFERENCES grupo(id)
);

CREATE TABLE album_artista(
id_album int,
id_artista int,
primary key(id_album, id_artista) 
);